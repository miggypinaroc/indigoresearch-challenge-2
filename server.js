const http = require("http");
const fs = require("fs");
const hostname = "localhost";
const port = 5000;

const server = http.createServer((req, res) => {
  console.log("request ", req.url);
  let filePath = "." + req.url;
  if (filePath == "./") {
    filePath = "index.html";
  }

  fs.exists(filePath, exists => {
    if (!exists) {
      res.statusCode = 404;
      return;
    }

    fs.readFile(filePath, (err, data) => {
      if (err) throw err;
      res.end(data, "utf-8");
    });

    // if (filePath.endsWith(".ttf")) {
    //   fs.readFile(filePath, (err, data) => {
    //     if (err) throw err;
    //     res.end(data, "binary");
    //   });
    // }
  });
});

server.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`);
});
