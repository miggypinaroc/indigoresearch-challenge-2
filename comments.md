# Comments

## File: server.js

Creating your own server is okay but not required for this project. On this note, I noticed the page never seems to finish loading. The server fails to server `.ttf` files correctly. 

## File: index.html

```html
<script src="./server.js"></script>
```
Like in the previous challenge, do not import your `server.js`.

## File: app.js

```js
data: () => {
    return {
        cardNum: 2,
        cardSuit: "♠️",
        developer: "Miggy Pinaroc",
        designer: "Mike del Castillo",
        isFlipped: true,
        isRed: false
    };
},
```

This portion can also be written like so.

```js
data: () => ({
    cardNum: 2,
    cardSuit: "♠️",
    developer: "Miggy Pinaroc",
    designer: "Mike del Castillo",
    isFlipped: true,
    isRed: false
}),
```
Arrow functions such as these `() => "hi"` is the same as `function(){ return "hi" }` from a `return` sense. If you want to return an object with an arrow function, you can wrap it in parentheses like this `() => ({object: true})`. However, the best way to shorten this is to define a method in the object like this.


```js
data(){
    return {
        cardNum: 2,
        cardSuit: "♠️",
        developer: "Miggy Pinaroc",
        designer: "Mike del Castillo",
        isFlipped: true,
        isRed: false
    }
},
```

**Follow instructions.** The card flip is good! However, a backside for the card was not specified. The instructions were to follow the brand guide as close as possible. 

The `setTimeout` parts in your code causes bugs. Read about `clearTimeout` to avoid cards staying in a flipped state.

## Desgin

Follow the provided brand guide. Fonts and font-weights were not followed correctly. Back side of the card was not in the brand guide. Read up about Google Fonts.

## End notes

Great work applying Vue directives, events, and methods. Read about brand guides and practice being keen to tiny details, especially when given a design to base your work off of. 

