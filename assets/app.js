new Vue({
  el: "#app",
  name: "RandomCardGenerator",
  data: () => {
    return {
      cardNum: 2,
      cardSuit: "♠️",
      developer: "Miggy Pinaroc",
      designer: "Mike del Castillo",
      isFlipped: true,
      isRed: false
    };
  },
  methods: {
    getRandomCard() {
      setTimeout(() => {
        this.getRandomNum();
        this.getRandomSuit();
        console.log(this.cardNum + " " + this.cardSuit);
      }, 250);
      this.flipCard();
    },
    getRandomNum() {
      let numList = ["A", 2, 3, 4, 5, 6, 7, 8, 9, 10, "J", "Q", "K"];
      let i = Math.floor(Math.random() * numList.length);
      this.cardNum = numList[i];
    },
    getRandomSuit() {
      let suitList = ["♣️", "♠️", "♥️", "♦️"];
      let i = Math.floor(Math.random() * suitList.length);

      if (i === 2 || i === 3) {
        this.isRed = true;
      } else {
        this.isRed = false;
      }

      this.cardSuit = suitList[i];
    },
    flipCard() {
      this.isFlipped = !this.isFlipped;
      if (this.isFlipped === false) {
        setTimeout(() => {
          this.isFlipped = true;
        }, 2000);
      }
    }
  }
});
